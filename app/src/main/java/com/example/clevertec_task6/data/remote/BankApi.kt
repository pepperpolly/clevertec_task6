package com.example.clevertec_task6.data.remote

import retrofit2.http.GET
import retrofit2.http.Query

interface BankApi {
    @GET("atm?")
    suspend fun getATMInfo(@Query("city") city: String): List<ATMInfoItem>

    @GET("infobox?")
    suspend fun getInfoboxInfo(@Query("city") city: String): List<InfoboxInfoItem>

    @GET("filials_info?")
    suspend fun getFilialsInfo(@Query("city") city: String): List<FilialInfoItem>
}