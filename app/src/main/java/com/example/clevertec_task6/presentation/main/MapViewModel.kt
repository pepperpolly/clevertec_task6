package com.example.clevertec_task6.presentation.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.clevertec_task6.common.DistanceCounter
import com.example.clevertec_task6.data.remote.CoordinatesItem
import com.example.clevertec_task6.data.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class MapViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    private val coordinates: MutableLiveData<MutableList<CoordinatesItem>> =
        MutableLiveData(mutableListOf())
    val coordinatesOpen = coordinates

    private var tempList = mutableListOf<CoordinatesItem>()
    private val distanceCounter = DistanceCounter

    fun getCoordinates(city: String) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                tempList.addAll(
                    repository.getATMInfo(city).map { CoordinatesItem(it.gps_x, it.gps_y) })
                tempList.addAll(
                    repository.getInfoboxInfo(city).map { CoordinatesItem(it.gps_x, it.gps_y) })
                tempList.addAll(
                    repository.getFilialsInfo(city).map { CoordinatesItem(it.GPS_X, it.GPS_Y) })
                tempList.sortBy {
                    distanceCounter.getDistance(it.gps_x.toDouble(), it.gps_y.toDouble())
                }
                coordinates.postValue(tempList.subList(0, 10))
            } catch (e: Exception) {
                println(e.stackTrace)
            }
        }
    }
}