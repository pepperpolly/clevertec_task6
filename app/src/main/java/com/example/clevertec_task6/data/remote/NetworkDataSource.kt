package com.example.clevertec_task6.data.remote

import javax.inject.Inject

class NetworkDataSource @Inject constructor(private val bankApi: BankApi) {

    suspend fun getATMInfo(city: String): List<ATMInfoItem> = bankApi.getATMInfo(city)

    suspend fun getInfoboxInfo(city: String): List<InfoboxInfoItem> = bankApi.getInfoboxInfo(city)

    suspend fun getFilialsInfo(city: String): List<FilialInfoItem> = bankApi.getFilialsInfo(city)
}