package com.example.clevertec_task6.di

import android.app.Application
import android.content.Context

class BankApplication : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
            .application(this)
            .build()
    }

}

val Context.appComponent: AppComponent
    get() = when (this) {
        is BankApplication -> this.appComponent
        else -> this.applicationContext.appComponent
    }