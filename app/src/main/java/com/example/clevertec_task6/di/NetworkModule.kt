package com.example.clevertec_task6.di

import com.example.clevertec_task6.data.remote.BankApi
import com.example.clevertec_task6.data.remote.NetworkDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule {
    @Provides
    @Singleton
    fun provideNetworkDataSource(
        bankApi: BankApi
    ): NetworkDataSource {
        return NetworkDataSource(bankApi)
    }
}