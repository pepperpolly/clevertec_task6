package com.example.clevertec_task6.common

import kotlin.math.max
import kotlin.math.min
import kotlin.math.pow
import kotlin.math.sqrt

private const val LAT = 52.425163
private const val LON = 31.015039

object DistanceCounter {
    fun getDistance(x: Double, y: Double): Double {
        return sqrt(
            (max(LAT, x) - min(
                LAT, x
            )).pow(2.0) + (max(
                LON,
                y
            ) - min(
                LON,
                y
            ))
                .pow(2.0)
        )
    }
}