package com.example.clevertec_task6.data.repository

import com.example.clevertec_task6.data.remote.ATMInfoItem
import com.example.clevertec_task6.data.remote.FilialInfoItem
import com.example.clevertec_task6.data.remote.InfoboxInfoItem
import com.example.clevertec_task6.data.remote.NetworkDataSource
import javax.inject.Inject

class Repository @Inject constructor(private val networkDataSource: NetworkDataSource) {

    suspend fun getATMInfo(city: String): List<ATMInfoItem> =
        networkDataSource.getATMInfo(city)

    suspend fun getInfoboxInfo(city: String): List<InfoboxInfoItem> =
        networkDataSource.getInfoboxInfo(city)

    suspend fun getFilialsInfo(city: String): List<FilialInfoItem> =
        networkDataSource.getFilialsInfo(city)
}