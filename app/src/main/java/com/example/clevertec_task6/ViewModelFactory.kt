package com.example.clevertec_task6

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.clevertec_task6.presentation.main.MapViewModel
import javax.inject.Inject
import javax.inject.Provider

class ViewModelFactory @Inject constructor(
    viewMainModelProvider: Provider<MapViewModel>
) :
    ViewModelProvider.Factory {
    private val providers =
        mapOf<Class<*>, Provider<out ViewModel>>(
            MapViewModel::class.java to viewMainModelProvider
        )

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return providers[modelClass]!!.get() as T
    }

}