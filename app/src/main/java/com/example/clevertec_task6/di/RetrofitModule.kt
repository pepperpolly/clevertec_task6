package com.example.clevertec_task6.di

import com.example.clevertec_task6.data.remote.BankApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class RetrofitModule {

    @Provides
    @Singleton
    fun provideWeatherApi(): BankApi {
        return Retrofit.Builder()
            .baseUrl("https://belarusbank.by/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(BankApi::class.java)
    }
}