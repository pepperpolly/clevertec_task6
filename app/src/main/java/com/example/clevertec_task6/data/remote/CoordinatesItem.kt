package com.example.clevertec_task6.data.remote

data class CoordinatesItem(
    val gps_x: String,
    val gps_y: String
)
