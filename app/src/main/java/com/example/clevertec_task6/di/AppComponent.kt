package com.example.clevertec_task6.di

import android.app.Application
import com.example.clevertec_task6.ViewModelFactory
import com.example.clevertec_task6.presentation.main.MapsActivity
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Component(modules = [RepositoryModule::class, RetrofitModule::class, NetworkModule::class])
@Singleton
interface AppComponent {

    fun viewModelsFactory(): ViewModelFactory

    fun inject(mapsActivity: MapsActivity)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}